-- sqlite
.mode csv
.separator "\t"
DROP TABLE IF EXISTS name_basics;
DROP TABLE IF EXISTS title_akas;
DROP TABLE IF EXISTS title_basics;
DROP TABLE IF EXISTS title_crew;
DROP TABLE IF EXISTS title_episode;
DROP TABLE IF EXISTS title_principals;
DROP TABLE IF EXISTS title_ratings;

.print "Importing name_basics"
CREATE TABLE name_basics(
  nconst TEXT,
  primaryName TEXT,
  birthYear INTEGER,
  deathYear INTEGER,
  primaryProfession TEXT,
  knownForTitles TEXT
);
.import data/name.basics.tsv name_basics
DELETE FROM name_basics WHERE nconst = 'nconst';

.print "Importing title_akas"
CREATE TABLE title_akas(
  "titleId" TEXT,
  "ordering" INTEGER,
  "title" TEXT,
  "region" TEXT,
  "language" TEXT,
  "types" TEXT,
  "attributes" TEXT,
  "isOriginalTitle" INTEGER
);
.import data/title.akas.tsv title_akas 
DELETE FROM title_akas WHERE titleId = 'titleId';

.print "Importing title_basics"
CREATE TABLE title_basics(
  "tconst" TEXT,
  "titleType" TEXT,
  "primaryTitle" TEXT,
  "originalTitle" TEXT,
  "isAdult" INTEGER,
  "startYear" INTEGER,
  "endYear" INTEGER,
  "runtimeMinutes" INTEGER,
  "genres" TEXT
);
.import data/title.basics.tsv title_basics
DELETE FROM title_basics WHERE tconst = 'tconst';

.print "Importing title_crew"
CREATE TABLE title_crew(
  "tconst" TEXT,
  "directors" TEXT,
  "writers" TEXT
);
.import data/title.crew.tsv title_crew
DELETE FROM title_crew  WHERE tconst = 'tconst';

.print "Importing title_episode"
CREATE TABLE title_episode(
  "tconst" TEXT,
  "parentTconst" TEXT,
  "seasonNumber" INTEGER,
  "episodeNumber" INTEGER
);
.import data/title.episode.tsv title_episode
DELETE FROM title_episode  WHERE tconst = 'tconst';

.print "Importing title_principals"
CREATE TABLE title_principals(
  "tconst" TEXT,
  "ordering" INTEGER,
  "nconst" TEXT,
  "category" TEXT,
  "job" TEXT,
  "characters" TEXT
);
.import data/title.principals.tsv title_principals
DELETE FROM title_principals  WHERE tconst = 'tconst';

.print "Importing title_ratings"
CREATE TABLE title_ratings(
  "tconst" TEXT,
  "averageRating" REAL,
  "numVotes" INTEGER
);
.import data/title.ratings.tsv title_ratings
DELETE FROM title_ratings  WHERE tconst = 'tconst';
