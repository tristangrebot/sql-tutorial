--sqlite
.headers on
.mode column

-- Filter table name_basics
.print "Creating imdb_filtered.sqlite"
ATTACH DATABASE "imdb_filtered.sqlite" as imdb_f;

-- Filter table title_basics
.print ""
.print "Importing title_basics filtered table"
DROP TABLE IF EXISTS imdb_f.title_basics;
CREATE TABLE imdb_f.title_basics AS
SELECT *
FROM main.title_basics tb
WHERE tb.startYear >= 1960
    AND tb.titleType IN ('movie', 'short') ;

SELECT "imdb.sqlite" db, count(*) FROM main.title_basics
UNION
SELECT "imdb_filtered.sqlite" db, count(*) FROM imdb_f.title_basics;

-- Filter table title_akas
.print ""
.print "Importing title_akas filtered table"
CREATE TABLE imdb_f.title_akas AS
SELECT ta.* 
FROM main.title_akas ta
JOIN imdb_f.title_basics tb
    ON ta.titleId = tb.tconst;

SELECT "imdb.sqlite" db, count(*) FROM main.title_akas
UNION
SELECT "imdb_filtered.sqlite" db, count(*) FROM imdb_f.title_akas;

-- Filter table title_crew
.print ""
.print "Importing title_crew filtered table"
CREATE TABLE imdb_f.title_crew AS
SELECT tc.*
FROM main.title_crew tc
JOIN imdb_f.title_basics tb
    ON tc.tconst = tb.tconst;

SELECT "imdb.sqlite" db, count(*) FROM main.title_crew
UNION
SELECT "imdb_filtered.sqlite" db, count(*) FROM imdb_f.title_crew;

-- Filter table title_principals
.print ""
.print "Importing title_principals filtered table"
CREATE TABLE imdb_f.title_principals AS
SELECT tp.*
FROM main.title_principals tp
JOIN imdb_f.title_basics tb
    ON tp.tconst = tb.tconst;

SELECT "imdb.sqlite" db, count(*) FROM main.title_principals
UNION
SELECT "imdb_filtered.sqlite" db, count(*) FROM imdb_f.title_principals;

-- Filter table title_ratings
.print ""
.print "Importing title_ratings filtered table"
CREATE TABLE imdb_f.title_ratings AS
SELECT tr.*
FROM main.title_ratings tr
JOIN imdb_f.title_basics tb
    ON tr.tconst = tb.tconst;

SELECT "imdb.sqlite" db, count(*) FROM main.title_ratings
UNION
SELECT "imdb_filtered.sqlite" db, count(*) FROM imdb_f.title_ratings;

-- Filter table name_basics
.print ""
.print "Importing name_basics filtered table"
CREATE TABLE imdb_f.name_basics AS
SELECT nb.*
FROM main.name_basics nb
JOIN (
    SELECT DISTINCT nconst
    FROM imdb_f.title_principals
    ) tp
    ON tp.nconst = nb.nconst;

SELECT "imdb.sqlite" db, count(*) FROM main.name_basics
UNION
SELECT "imdb_filtered.sqlite" db, count(*) FROM imdb_f.name_basics;
