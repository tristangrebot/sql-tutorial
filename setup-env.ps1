
Function DeGZip-File{
    Param(
        $infile,
        $outfile = ($infile -replace '\.gz$','')
        )

    $input = New-Object System.IO.FileStream $inFile, ([IO.FileMode]::Open), ([IO.FileAccess]::Read), ([IO.FileShare]::Read)
    $output = New-Object System.IO.FileStream $outFile, ([IO.FileMode]::Create), ([IO.FileAccess]::Write), ([IO.FileShare]::None)
    $gzipStream = New-Object System.IO.Compression.GzipStream $input, ([IO.Compression.CompressionMode]::Decompress)

    $buffer = New-Object byte[](1024)
    while($true){
        $read = $gzipstream.Read($buffer, 0, 1024)
        if ($read -le 0){break}
        $output.Write($buffer, 0, $read)
        }

    $gzipStream.Close()
    $output.Close()
    $input.Close()
}

# Clean data\temp folder
Remove-Item "data\temp\" -Filter * -Recurse -ErrorAction Ignore
New-Item -ItemType directory -Path data\temp\


$url = "https://datasets.imdbws.com/"
$files = @("title.ratings.tsv.gz", "name.basics.tsv.gz", "title.akas.tsv.gz", "title.basics.tsv.gz", "title.crew.tsv.gz", "title.episode.tsv.gz", "title.principals.tsv.gz")

Import-Module BitsTransfer
foreach ($file in $files){
    Write-Output "Download $file"
    $start_time = Get-Date
    Start-BitsTransfer -Source $url$file -Destination data\temp\$file
    Write-Output "Time taken: $((Get-Date).Subtract($start_time).Seconds) second(s)"
}

Get-ChildItem data\temp | ForEach-Object{
    DeGZip-File $_.FullName (($_.FullName -replace '\.gz$','') -replace 'temp\\','')
}

Get-ChildItem data\*.tsv | ForEach-Object{
    $File = $_.FullName
    Write-Output "$File"
    (Get-Content $File) | Foreach-Object {$_ -replace '"', "'"} | Set-Content $File
}