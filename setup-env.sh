#!/bin/bash
 
# Declare an array of string with type
url="https://datasets.imdbws.com/"
files=("title.ratings.tsv.gz" "name.basics.tsv.gz" "title.akas.tsv.gz" "title.basics.tsv.gz" "title.crew.tsv.gz" "title.episode.tsv.gz" "title.principals.tsv.gz")

# Iterate the string array using for loop
for file in ${files[@]}; do
   if [ ! -f "data/${file/.gz/}" ]; then
      if [ ! -f "data/temp/${file/.gz/}" ]; then
         link="$url$file"
         echo "Downloading $link" 
         curl -L -o data/temp/$file --create-dirs -C - $link
         gunzip data/temp/$file
      fi
   fi
done

if [ $(find data/temp -maxdepth 1 -name '*.tsv' | wc -l) -gt 0 ]; then
   for file in data/temp/*.tsv; do
      echo "Cleaning $file"
      sed -i "s/\"/'/g" $file
      mv $file data/
   done
fi

sqlite3 imdb.sqlite ".read create_db.sql"
sqlite3 imdb.sqlite ".read filter_db.sql"